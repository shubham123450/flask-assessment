# Contact CRUD application

Create a Flask application to manage contacts of a user.

Use Cases:

- User can add a new contact by adding 
    - Name
    - Phone Number
    - Email
- User can view all the contacts added in a tabular view
- User can delete a contact 
- User can edit a contact
- User can search contacts by name and email only

Guidelines:

- Use Jinja template for templating the application
- Use bootstrap for responsive UIs (https://getbootstrap.com/)
- Add basis validation on all the fields (name, email and phone number)
