
# Overview

Create a REST APIs to be consumed by a mobile app for storing contacts using mobile app.  

# Technology Stack

You need to use Flask for creating the API. And use MySQL or PostgreSQL as database.

# Terminology and assumptions:

* User can signup into application using email, name and password.
* User can login into the application using email and password.
* Each registered user of the app can have zero or more personal "Contacts".
* The UI will be built by someone else - you are simply making the REST API endpoints to be
consumed by the front end.
* You will be writing the code as if it’s for production use and should thus have the required
performance and security. 

# APIs required
1. API to signup using name, email and password.
2. API to login into the application
3. API to get user's details
4. API to create new contact
5. API to list app the contacts
6. API to search a contact by:
    * Name
    * Email
    * Phone

# Database

## User
Following information required for each user registered on the application
* Name
* Phone Number
* Email
* Address

## Contacts
Each user can store as many as contact in hi profile with following information
* Name
* Email
* Phone
* Address
* Country

# Evaluation criteria:
* Completeness of functionality
* Correctness under thorough testing
* Performance and scalability of APIs
* Security of APIs
* Data modeling
* Structure of code
* Readability of code

# Deployment:
* Deploy the APIs on any of the public URL

# Assignment Submition:
* Push the assignment on Gitlab or Github and share the URL as submission
* Share the postman collection along with the Git URL
* Share the public URL of the application

