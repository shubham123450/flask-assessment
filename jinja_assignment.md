# Jinja Template Assignment

Create this HTML view using Flask and Jinja

![template](template_preview.png "Template")

Guidelines:

- The date should be a the today's date
- You can use this url for user's photo: https://i.pravatar.cc/300
- You can use this URL for random photos: https://picsum.photos/
